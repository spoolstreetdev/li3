import Adaptable from "../core/adaptable";

const sequelize = require("sequelize");

export default new class extends Adaptable {
	protected _configurations = {};

	get = (name: string = null) => {
		if (!name) {
			let keys = Object.keys(this._configurations);

			if (!keys.length) {
				return null;
			}

			name = keys[0];
		}

		let config = this.config(name);

		switch (config.type) {
			case "database":
				if (!config.object) {
					config.object = new sequelize(config.database, config.login, config.password, {
						define: {
							freezeTableName: true,
							underscored: true
						},

						host: config.host || "127.0.0.1",
						logging: !!config.debug,
						dialect: "mysql"
					});
				}

				break;
		}

		return config.object;
	};
}