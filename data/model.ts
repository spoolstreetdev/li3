import Throwable from "../core/throwable";
import Libraries from "../core/libraries";
import Collection from "../util/collection";
import Set from "../util/set";
import Filters from "../aop/filters";
import Inflector from "../util/inflector";

import Connections from "./connections";

const sequelize = require("sequelize");

export default class Model {
	protected _object;
	protected _data;

	table: string;
	fields: any;

	config?: {
		timestamps: { created: string|boolean, updated: string|boolean } | boolean;
	};

	belongsTo: Array<string> = [];
	hasMany: Array<string> = [];

	static create = async function(data?: any, options: { commit: boolean } = { commit: false }) {
		let object = new this(data);
		object.init();

		if (options.commit) {
			return await object.save();
		}

		return object;
	};

	constructor(data?: any) {
		if (data) {
			this._data = data;
		}
	}

	init = () => {
		let fields = ["id"];

		if ("fields" in this) {
			for (let field in this.fields) {
				fields.push(field);
			}
		}

		if ("config" in this) {
			if ("timestamps" in this.config && this.config.timestamps) {
				if (this.config.timestamps === true) {
					fields.push("created");
					fields.push("updated");
				}

				if (typeof this.config.timestamps == "object") {
					if (this.config.timestamps.created === true) {
						fields.push("created");
					} else if (typeof this.config.timestamps.created === "string") {
						fields.push(this.config.timestamps.created);
					}

					if (this.config.timestamps.updated === true) {
						fields.push("updated");
					} else if (typeof this.config.timestamps.updated === "string") {
						fields.push(this.config.timestamps.updated);
					}
				}
			}
		}

		if (this.belongsTo.length) {
			this.belongsTo.forEach((name) => {
				fields.push(Inflector.singularize(name).toLowerCase());
			});
		}

		fields.forEach((field) => {
			if (!this.hasOwnProperty(field)) {
				Object.defineProperty(this, field, {
					get: function() {
						return this._data[field];
					},

					set: function(value) {
						this._data[field] = value;
					}
				});
			}
		});

		return true;
	};

	find = async (options: any) => {
		if (typeof options === "number" || (typeof options === "object" && typeof options.type === "string" && options.type.match(/^\d+$/))) {
			options = { conditions: { id: typeof options === "number" ? options : options.type } };
		}

		if (typeof options !== "object") {
			throw new Error("Invalid find options.");
		}

		if (!options.type) {
			options.type = "one";
		}

		switch (options.type) {
			case "one":
				let result = await this.object.findOne(this._options(options));

				if (!result) {
					return !options.entity ? false : await (<typeof Model> this.constructor).create(options.conditions || {});
				}

				let data = Set.expand(result);

				if (options.entity) {
					return await (<typeof Model> this.constructor).create(data);
				}

				return data;
			case "all":
				let results = await this.object.findAll(this._options(options));

				if (!results.length) {
					return false;
				}

				if (options.entity) {
					return await Promise.all(results.map(async (value) => (await (<typeof Model> this.constructor).create(Set.expand(value)))));
				}

				return results.map((value) => Set.expand(value));
			default:
				throw new Throwable("Unknown find type.", 500);
		}
	};

	save = async () => {
		return await Filters.run(Model, "save", this, async (instance) => {
			if (instance.id) {
				await this.object.update(Collection.extract(["id"], instance.data, { exclusive: true }), this._options({ conditions: { id: instance.id } }));
				return instance;
			}

			return await ((<typeof Model> this.constructor).create(Object.assign({}, instance.data, { id: (await this.object.create(instance.data, { raw: true })).id })));
		});
	};

	get data() {
		return this._data;
	}

	get object() {
		if (!this._object) {
			let fields: any = {},
				config: any = {};

			if ("fields" in this) {
				for (let index in this.fields) {
					fields[index] = sequelize[this.fields[index].toUpperCase()];
				}
			}

			if ("config" in this) {
				if (this.config.timestamps) {
					let tsconfig: { timestamps: boolean, createdAt: string|boolean, updatedAt: string|boolean }|any = {};

					if (this.config.timestamps === true) {
						Object.assign(tsconfig, { timestamps: true, createdAt: "created", updatedAt: "updated" });
					}

					if (typeof this.config.timestamps == "object") {
						tsconfig.timestamps = true;

						if (this.config.timestamps.created === true) {
							tsconfig.createdAt = "created";
						} else if (typeof this.config.timestamps.created === "string") {
							tsconfig.createdAt = this.config.timestamps.created;
						}

						if (this.config.timestamps.updated === true) {
							tsconfig.updatedAt = "updated";
						} else if (typeof this.config.timestamps.updated === "string") {
							tsconfig.updatedAt = this.config.timestamps.updated;
						}
					}

					Object.assign(config, tsconfig);
				}
			}

			if (!("timestamps" in config)) {
				config.timestamps = false;
			}

			this._object = Connections.get().define(this.table, fields, config);

			if (this.belongsTo.length) {
				this.belongsTo.forEach((name) => {
					this._object.belongsTo(Libraries.load({ name, type: "models" }).object);
				});
			}

			if (this.hasMany.length) {
				this.hasMany.forEach((name) => {
					this._object.hasMany(Libraries.load({ name, type: "models" }).object);
				});
			}
		}

		return this._object;
	}

	_options = (params: any) => {
		let map = {
			fields: "attributes",
			conditions: "where",
			group: "group",
			limit: "limit",
			required: "required"
		};

		let options: any = {
			raw: true
		};

		for (let key in params) {
			if (map[key]) {
				switch (key) {
					case "conditions":
						let conditions = {};

						if (key in params) {
							for (let name in params[key]) {
								if (typeof params[key][name] == "object") {
									let field = {};

									for (let operator in params[key][name]) {
										field[this._operator(operator)] = params[key][name][operator];
									}

									conditions[name] = field;
								} else if (params[key][name] === null) {
									let value = {};
									value[this._operator("=")] = null;
									conditions[name] = value;
								} else {
									conditions[name] = params[key][name];
								}
							}
						}

						options[map[key]] = conditions;
						break;
					default:
						options[map[key]] = params[key];
						break;
				}
			}
		}

		if (params.with) {
			options.include = this._with(params.with);
		}

		if (params.order) {
			options.order = [];

			for (let key in params.order) {
				options.order.push([key, params.order[key]]);
			}
		}

		return options;
	};

	_operator = (operator) => {
		switch (operator) {
			case "or":
				return sequelize.Op.or;
			case "and":
				return sequelize.Op.and;
			case "=":
				return sequelize.Op.eq;
			case ">=":
				return sequelize.Op.gte;
			case ">":
				return sequelize.Op.gt;
			case "<=":
				return sequelize.Op.lte;
			case "<":
				return sequelize.Op.lt;
			case "!=":
				return sequelize.Op.ne;
			case "not":
				return sequelize.Op.not;
			case "like":
				return sequelize.Op.like;
			case "^like":
				return sequelize.Op.notLike;
			case "pattern":
				return sequelize.Op.regexp;
			case "^pattern":
				return sequelize.Op.notRegexp;
			case "in":
				return sequelize.Op.in;
			case "^in":
				return sequelize.Opt.notIn;
		}

		throw new Error("Unimplemented operator");
	};

	_with = (params: any) => {
		let include = [];

		if (params instanceof Array) {
			params.forEach((name) => {
				if (typeof name == "string") {
					include.push({
						model: Libraries.load({ name, type: "models" }).object
					});
				}
			});
		} else if (typeof params == "object") {
			for (let name in params) {
				include.push(Object.assign({}, this._options(params[name]) || {}, {
					model: Libraries.load({ name, type: "models" }).object
				}));
			}
		}

		return include.length > 0 ? include : null;
	};
}