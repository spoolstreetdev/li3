import Model from "../model";

export default class extends Model {
	value = async (params: { conditions: any, group?: boolean }) => await this._value(params);

	protected _value = async (params: { conditions: any, group?: boolean }) => {
		let defaults = {
			conditions: null,
			group: false,
			cascade: false
		};

		params = Object.assign({}, defaults, params);

		let result,
			fields = ["key", "value"],
			conditions = Object.assign({}, params.conditions, { key: !params.group ? params.conditions.key : { like: `${params.conditions.key}.%` }});

		if (typeof params.conditions.key == "string" && !params.group) {
			result = await this.find({
				fields,
				conditions
			});

			return result !== null ? result.value : false;
		}

		if ((params.conditions.key instanceof Array) || (typeof params.conditions.key == "string" && params.group)) {
			result = await this.find({
				type: "all",
				fields,
				conditions
			});

			if (!result) {
				return false;
			}

			let map = {};

			result.forEach((setting) => {
				map[!params.group ? setting.key : setting.key.split(".").slice(-1).join(".")] = setting.value;
			});

			return map;
		}

		return null;
	}
}