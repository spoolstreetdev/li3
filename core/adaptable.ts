export default class Adaptable {
	protected _configurations = {};

	config = (config?: any, options: any = {}) => {
		if (!("name" in options)) {
			options.name = "default";
		}

		if (config) {
			switch (typeof config) {
				case "string":
					return (config in this._configurations) ? this._configurations[config] : false;
				case "object":
					if (options.name) {
						return this._configurations[options.name] = config;
					}

					return this._configurations = config;
				default:
					return false;
			}
		}

		return this._configurations;
	};

	set = (name: string, value: any, options: any = {}) => {
		options = this._options(options);

		if (!(options.name in this._configurations)) {
			return false;
		}

		return this._configurations[options.name][name] = value;
	};

	add = (name: string, config: any = {}) => {
		return this._configurations[name] = config;
	};

	remove = (options: any = {}) => {
		options = this._options(options);

		if (options.name in this._configurations) {
			delete this._configurations[options.name];
		}
	};

	get = (name: string, options: any = {}) => {
		options = this._options(options);

		if (!(options.name in this._configurations) || !(name in this._configurations[options.name])) {
			return false;
		}

		return this._configurations[options.name][name];
	};

	_options = (options: any) => {
		if (!options.name) {
			options.name = "default";
		}

		return options;
	};
}