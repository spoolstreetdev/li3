import Filters from "../aop/filters";

export class Handler {
	args: any = {};
	chain: Promise<any>;

	static get = async (params) => {
		try {
			return await Filters.run(Handler, "get", params, async () => {
				throw new Error("No event handlers.");
			});
		} catch (error) {
			console.error(error);
		}
	};

	constructor(args: any) {
		for (let key in args) {
			this.args[key] = args[key];
		}
	}
}

export default Handler;

export interface Abstract {
	extract();

	done(params?: any);
	error(params?: any);
}