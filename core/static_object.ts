import Collection from "../util/collection";

export class StaticObject {
	protected static _config: any = {};

	constructor(params?: any) {
		if (params) {
			for (let key in params) {
				this[key] = params[key];
			}
		}
	}

	static config(config?: any): any {
		if (!config) {
			return this._config;
		}

		return this._config = Collection.merge(this._config, config);
	}
}

export default StaticObject;