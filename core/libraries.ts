import Text from "../util/text";
import Throwable from "./throwable";
import Inflector from "../util/inflector";
import StaticObject from "./static_object";

const fs = require("fs");
const path = require("path");

export class Libraries extends StaticObject {
	protected static _paths = {
		controllers: [
			"{:library}/controllers/{:name}"
		],

		invocations: [
			"{:library}/invocations/{:name}"
		],

		models: [
			"{:library}/models/{:name}"
		],

		handlers: [
			"{:library}/{:type}/handler/{:name}"
		],

		components: [
			"{:library}/extensions/components/{:name}"
		],

		storage: [
			"{:library}/net/http/storage/{:name}"
		]
	};

	protected static _config: any = {};
	protected static _configurations = {};

	static add(name: string, config: any = {}) {
		let defaults = {
			path: null,
			default: false,
			bootstrap: true
		};

		config = Object.assign({}, defaults, config);

		if (config.default) {
			config.path = path.resolve();
		}

		if (!config.path) {
			config.path = path.resolve("node_modules", name);
		}

		if (config.default || config.path == path.resolve("node_modules", "li3")) {
			config.bootstrap = false;
		}

		if (config.bootstrap) {
			let bootstrap;

			if (config.bootstrap === true) {
				bootstrap = "config/bootstrap";
			} else if (typeof config.bootstrap == "string") {
				bootstrap = config.bootstrap;
			}

			if (bootstrap && fs.existsSync(`${config.path}/${bootstrap}.js`)) {
				require(`${config.path}/${bootstrap}`);
			}
		}

		return this._configurations[name] = config;
	}

	static get(name?: string) {
		if (!name) {
			return this._configurations;
		}

		return this._configurations[name] || false;
	}

	static update(name: string, config) {
		if (!this._configurations[name]) {
			return false;
		}

		return this._configurations[name] = Object.assign({}, this._configurations[name], config);
	}

	static paths(name?: string) {
		return !name ? this._paths : (this._paths[name] || false);
	}

	static load(params: any, config: any = { import: "default" }) {
		let module = require(this._path(params));

		if (!config.import || !module[config.import]) {
			return module;
		}

		return module[config.import];
	}

	protected static _path(params: any) {
		let paths = this.paths();

		if (!(params.type in paths) || !(paths[params.type] instanceof Array)) {
			throw new Throwable(`Module type '${params.type}' not found.`, 500);
		}

		let resolver = (library) => {
			for (let index = 0; index < paths[params.type].length; index++) {
				let path = Text.insert(paths[params.type][index], Object.assign({}, params, { library, name: params.name ? Inflector.underscore(params.name) : null }));

				try {
					return require.resolve(path);
				} catch (error) {
				}
			}

			return false;
		};

		let libraries: any[];

		if (!params.library) {
			libraries = this.get();
		} else {
			libraries = [params.library];
		}

		for (let library in libraries) {
			if (libraries[library].delegates && libraries[library].delegates[params.type] && libraries[library].delegates[params.type].indexOf(params.name) !== -1) {
				continue;
			}

			let path;

			if ((path = resolver(libraries[library].path))) {
				return path;
			}
		}

		throw new Throwable(`Module '${params.name}' of type '${params.type}' not found.`, 500);
	}
}

export default Libraries;