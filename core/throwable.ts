export class Throwable extends Error {
	code: number = 400;

	constructor(message, code = 400) {
		super(message);

		this.code = code;
	}
}

export default Throwable;