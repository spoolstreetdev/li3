import * as child from "child_process";
import * as parser from "http-string-parser";

import {Response} from "./response";

export default class {
	static run (params: any = {}) {
		const defaults = {
			name: null,
			args: null,
			request: null,
			env: null
		};

		params = Object.assign({}, defaults, params);

		return new Promise((resolve, reject) => {
			let binary = child.spawn(params.name, [].concat(params.args || []).concat(process.env.args ? process.env.args.split(/\s+/) : []), {
				env: Object.assign({}, process.env, params.env)
			});

			(<any> binary.stdin).setEncoding("utf-8");

			let stdout = "";

			if (params.request.body) {
				let body = params.request.body,
					form = "application/x-www-form-urlencoded";

				if (!params.request.type()) {
					params.request.type(form);
				}

				const type = params.request.type().split(";")[0];

				switch (type) {
					case "multipart/form-data":
						body = Buffer.from(body, "base64").toString("binary");
						break;
				}

				binary.stdin.write(body);
			}

			binary.stdin.end();

			binary.stdout.on("data", (data: Buffer) => {
				stdout += data.toString("binary");
			});

			binary.stderr.on("data", (data: Buffer) => {
				console.error(data.toString("binary"));
			});

			binary.on("close", () => {
				let response = {
					headers: {},
					body: null
				};

				let http = parser.parseResponse(stdout);

				if (http.headers) {
					response.headers = http.headers;
				}

				try {
					let body = null;

					try {
						if (body = JSON.parse(http.body)) {
							switch (typeof body) {
								case "object":
								case "boolean":
								case "number":
									response.body = JSON.stringify(body);
									break;
								case "string":
									if (body) {
										response.body = body;
									}

									break;
							}
						}
					} catch (error) {
						response.body = http.body;
					}

					return resolve(new Response(response));
				} catch (error) {
					return reject(error);
				}
			});

			binary.on("error", (data: Buffer) => {
				reject(new Error(data.toString("utf-8")));
			});
		});
	}
}