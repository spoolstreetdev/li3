import * as request from "../net/http/request";

const defaults = {
	params: {},
	args: [],
	data: {}
};

export class Request extends request.Request {
	params: any| { controller: string, action: string };
	args: Array<string>;
	data: any;

	init(config: any) {
		super.init(Object.assign({}, defaults, config));

		if (this.path) {
			let parts = this.path.substr(1).split("/"),
				{0: controller, 1: action} = parts;

			Object.assign(this.params, {controller, action});
			this.args = parts.slice(2);
		}

		if (this.body) {
			this.data = this._decode(this.body);
		}
	}

	render(data = null, options = {}) {
		return super.render(data, options);
	}
}