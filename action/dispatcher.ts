import Filters from "../aop/filters";
import Libraries from "../core/libraries";
import Throwable from "../core/throwable";
import Inflector from "../util/inflector";
import Text from "../util/text";

export class Dispatcher {
	static async run(type: string, options: { request: any, types: { request: any, response: any } }) {
		let name = Inflector.singularize(type);

		if (!options.request.params[name].match(/^[a-z][a-z0-9]+$/)) {
			throw new Throwable(Text.insert("Invalid {:name} specified.", { name }), 400);
		}

		let target = new (Libraries.load({ name: options.request.params[name], type }))(options);

		if (!(options.request.params.action in target) || options.request.params.action in (() => {}) || typeof target[options.request.params.action] != "function") {
			throw new Throwable("Invalid action specified.", 404);
		}

		if (process.env.TRACE) {
			console.info(JSON.stringify(options.request, null ,2));
		}

		let params = {};
		params[name] = target;

		return await Filters.run(this, "run", Object.assign({}, params, { options }), async (params) => {
			let response;

			if (!((response = await params[name].invoke()) instanceof options.types.response)) {
				return new options.types.response();
			}

			return response;
		});
	}
}

export default Dispatcher;