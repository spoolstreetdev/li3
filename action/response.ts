import * as response from "../net/http/response";

const defaults = {
	options: null
};

export class Response extends response.Response {
	options: any = {
		encode: true
	};

	init(config: any) {
		super.init(Object.assign({}, defaults, config));
	}

	render(data = null, options = {}) {
		return super.render(data, Object.assign({}, { encode: true }, this.options || {}, options));
	}
}