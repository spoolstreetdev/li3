import Filters from "../aop/filters";
import Auth from "../security/auth";
import Throwable from "../core/throwable";
import Validator from "../util/validator";
import Action from "../action";

import {Request} from "../action/request";
import {Response} from "../action/response";

export default abstract class Controller extends Action {
	protected request: Request;
	protected validates: any = {};

	protected _headers = {};
	protected _statusCode = 200;

	raw: string[]|boolean = false;

	invoke = async () => await this._invoke();

	redirect = (to: string, code = 302) => {
		this.statusCode = code;
		this.headers.Location = to;

		return new Response({
			headers: this.headers,
			status: { code: this.statusCode },
		});
	};

	protected _invoke = async () => {
		if (typeof this.validates == "object" && this.request.params.action in this.validates && this.validates[this.request.params.action]) {
			this._validate(this.validates[this.request.params.action]);
		}

		return await Filters.run(Controller, "invoke", this, async (instance) => {
			let body = await instance[instance.request.params.action]();

			if (body != null) {
				if (body instanceof Response) {
					return body;
				} else if (!this.raw || (typeof this.raw != "object" || !(this.raw instanceof Array) || this.raw.indexOf(instance.request.params.action) === -1)) {
					return new Response({ headers: this.headers, status: { code: this.statusCode }});
				}
			}

			return new Response({
				headers: instance.headers,
				status: { code: instance.statusCode },
				body
			});
		});
	};

	protected _validate = (schema, data = this.request.data) => {
		let errors, auth;

		if (schema.type === "object" && (auth = Auth.config()) && auth.request_data_name) {
			schema.properties[auth.request_data_name] = {
				type: "string"
			};
		}

		if ((errors = Validator.run(data, schema)) !== true) {
			throw new Throwable(errors[0], 400);
		}

		return true;
	};

	set headers(headers: any) {
		this._headers = headers;
	}

	set statusCode(status: number) {
		this._statusCode = status;
	}

	get headers(): any {
		return this._headers;
	}

	get statusCode(): number {
		return this._statusCode;
	}
}