import Adaptable from "../core/adaptable";
import Libraries from "../core/libraries";

import Filters from "../aop/filters";
import Throwable from "../core/throwable";
import Validator from "../util/validator";

const jsonwebtoken = require("jsonwebtoken");
const tokenschema = require("../util/validator/schema/net/http/auth/token.json");

export class Auth extends Adaptable {
	check = async (name: string, params: any = {}) => {
		let defaults = {
			config: this.config(name),
			request: null,
			type: null,
			token: null
		};

		params = Object.assign({}, defaults, params);

		if (!params.request && !params.type && !params.token) {
			throw new Throwable("You must specify an authentication request or type-token pair.");
		}

		return await Filters.run(Auth, "check", params, async (result) => {
			return await this.verify(result);
		});
	};

	protected verify = async (result) => {
		if (!result.type) {
			throw new Throwable("Authentication type required.");
		}

		if (!result.token) {
			throw new Throwable("Authentication token required.");
		}

		let ProjectProperties = Libraries.load({ name: result.config.models ? result.config.models.ProjectProperties : "project_properties", type: "models" }),
			Projects = Libraries.load({ name: result.config.models ? result.config.models.Projects : "projects", type: "models" }),

			project,
			property;

		switch (result.type) {
			case "Bearer":
				if (!result.token) {
					break;
				}

				property = await ProjectProperties.find({
					conditions: {
						key: result.config.key,
						value: result.token
					}
				});

				if (!property) {
					throw new Throwable("Authentication token invalid.", 401);
				}

				project = await Projects.find({
					conditions: {
						id: property.project_id,
						state: { ">": 0 }
					},

					with: {
						Clients: {
							conditions: {
								active: true
							},

							with: ["Scopes"]
						}
					}
				});

				if (!project) {
					throw new Throwable("Project state invalid.", 403);
				}

				return project;
			case "JWT":
				if (!result.token) {
					break;
				}

				let payload = jsonwebtoken.decode(result.token);
				Validator.run(payload, tokenschema);

				project = await Projects.find({
					conditions: {
						uuid: payload.iss,
						state: { ">": 0 }
					},

					with: {
						Clients: {
							conditions: {
								active: true
							},

							with: ["Scopes"]
						}
					}
				});

				if (!project) {
					throw new Throwable("Issuer or project state invalid.", 401);
				}

				property = await ProjectProperties.find({
					conditions: {
						project_id: project.id,
						key: result.config.key
					},

					fields: ["value"]
				});

				if (!property) {
					throw new Throwable("Authentication source error.", 403);
				}

				jsonwebtoken.verify(result.token, property.value);

				if (result.request) {
					if (!payload.scope) {
						throw new Throwable("Unauthorized (no scope).", 401);
					}

					let authorized = false;

					Object.keys(payload.scope).forEach((controller) => {
						if (controller == result.request.params.controller && payload.scope[controller].indexOf(result.request.params.action) !== -1) {
							authorized = true;
						}
					});

					if (!authorized) {
						throw new Throwable("Unauthorized (invalid scope).", 401);
					}
				}

				return Object.assign({}, project, {
					project_id: project.id,
					external_user_id: payload.sub
				});
		}

		throw new Throwable("Authentication error.", 401);
	};
}

export default new Auth();