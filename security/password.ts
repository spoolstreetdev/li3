const bcrypt = require("bcryptjs");
const randomstring = require("randomstring");

const BF = 12;

export default class {
	static hash(password: string, salt: string = null) {
		return bcrypt.hashSync(password, salt || this.salt());
	}

	static check(password: string, hash: string) {
		return bcrypt.compareSync(password, hash);
	}

	static generate(length: number = 9) {
		return randomstring.generate({ length, readable: true });
	}

	static salt() {
		return bcrypt.genSaltSync(BF);
	}
}