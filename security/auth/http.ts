import Filters from "../../aop/filters";
import {Auth} from "../auth";

Filters.apply(Auth, "check", async (params, chain) => {
	if (params.request && !params.type && !params.token) {
		if (params.request.headers && params.request.headers.Authorization && params.request.headers.Authorization.split(" ").length == 2) {
			let { 0: type, 1: token } = params.request.headers.Authorization.split(" ");

			Object.assign(params, { type, token });
		} else if (params.request.data && params.config.request_data_name && params.config.request_data_name in params.request.data && typeof params.request.data[params.config.request_data_name] === "string") {
			Object.assign(params, { type: params.request.data[params.config.request_data_name].split(".").length !== 3 ? "Bearer" : "JWT", token: params.request.data[params.config.request_data_name] });
		}
	}

	return await chain.next(params);
});