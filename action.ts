export abstract class Action {
	protected request: any;

	constructor(config?: any) {
		if (config) {
			for (let key in config) {
				this[key] = config[key];
			}
		}
	}

	abstract invoke = async () => {};
}

export default Action;