import Iterator from "../util/collection/iterator";

export class Chain {
	protected _filters: Iterator;
	protected _implementation: any;

	constructor(config: any) {
		this._filters = new Iterator(config.filters);
	}

	run(params, implementation) {
		this._implementation = implementation;

		let filter = this._filters.reset();
		let result = filter(params, this);

        // why nullify?
		// this._implementation = null;

		return result;
	}

	next(params) {
		let filter,
			implementation;

		if ((filter = this._filters.next()) !== false) {
			return filter(params, this);
		}

		implementation = this._implementation;
		return implementation(params);
	}
}

export default Chain;