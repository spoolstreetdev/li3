import Chain from "./chain";

export class Filters {
	protected static _filters: Array<any>;
	protected static _chains: Array<Chain>;
	protected static _classIds = [];

	static apply(module, method, filter) {
		let id = this._ids(module, method)[0];

		if (!this._filters) {
			this._filters = [];
		}

		if (!this._filters[id]) {
			this._filters[id] = [];
		}

		this._filters[id].push(filter);

		if (!this._chains) {
			this._chains = [];
		}

		if (this._chains[id]) {
			this._chains = this._chains.splice(this._chains.indexOf(this._chains[id]), 1);
		}
	}

	static hasApplied(module, method) {
		let ids = this._ids(module, method);

		if (!this._filters) {
			this._filters = [];
		}

		for (let key in ids) {
			let id = ids[key];

			if (this._filters[id]) {
				return true;
			}
		}

		return false;
	}

	static run(module, method, params, implementation = (params) => { return params; }) {
		if (!this.hasApplied(module, method)) {
			return implementation(params);
		}

		return this._chain(module, method).run(params, implementation);
	}

	static _ids(module, method) {
		if (!("_filter_id" in module)) {
			while (true) {
				let random = Math.random();

				if (this._classIds.indexOf(random) === -1) {
					module._filter_id = random;
					this._classIds.push(random);
					break;
				}
			}
		}

		return [
			`<${module._filter_id}>::${method}`
		];
	}

	static _chain(module, method) {
		let ids = this._ids(module, method),
			filters = [];

		if (!this._chains) {
			this._chains = [];
		}

		if (this._chains[ids[0]]) {
			return this._chains[ids[0]];
		}

		ids.forEach((id, index, array) => {
			if (this._filters[id]) {
				filters = filters.concat(this._filters[id]);
			}
		});

		return this._chains[ids[0]] = new Chain({ filters });
	}
}

export default Filters;