export class Iterator {
	protected _data: Array<any>;
	protected _pointer: number;

	constructor(data) {
		this._data = data;
		this.reset();
	}

	reset(): any {
		return this._data[this._pointer = 0];
	}

	next(): any {
		let item = this._data[++this._pointer];

		if (item) {
			return item;
		}

		return false;
	}
}

export default Iterator;