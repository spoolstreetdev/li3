import Set from "./set";

export class Text {
	static insert(value: string, params: any) {
		let text = value,
			data = Set.flatten(params);

		for (let key in data) {
			if (data[key]) {
				text = text.replace(`{:${key}}`, data[key]);
			}
		}

		return text;
	}
}

export default Text;