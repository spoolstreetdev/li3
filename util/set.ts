const flat = require("flat");

export class Set {
	static flatten(data) {
		return flat(data);
	}

	static expand(data) {
		return flat.unflatten(data);
	}
}

export default Set;