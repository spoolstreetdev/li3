export class Collection {
	static merge(... objects): any {
		let collection = {};

		objects.forEach((object) => {
			for (let key in object) {
				let value = object[key];

				if (!(key in collection)) {
					collection[key] = value;
				} else if (collection[key] instanceof Array) {
					(value instanceof Array ? value : [value]).forEach((push) => {
						collection[key].push(push);
					});
				} else if (typeof collection[key] == "object") {
					collection[key] = Object.assign({}, collection[key], value);
				}
			}
		});

		return collection;
	}

	static extract(keys: Array<string>, object: any, options = { exclusive: false }): any {
		let data = {};

		for (let key in object) {
			if ((!options.exclusive && keys.indexOf(key) !== -1) || (options.exclusive && keys.indexOf(key) === -1)) {
				data[key] = object[key];
			}
		}

		return data;
	}
}

export default Collection;