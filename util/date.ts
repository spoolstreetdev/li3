export default class {
	static modify(term_frequency: number, term_unit: string, from: Date = new Date()) {
		let next = new Date(from);

		switch (term_unit) {
			case "year":
				next.setFullYear(next.getFullYear() + term_frequency);
				break;
			case "month":
				next.setMonth(next.getMonth() + term_frequency);
				break;
			case "week":
				next.setDate(next.getDate() + (term_frequency * 7));
				break;
			case "day":
				next.setDate(next.getDate() + term_frequency);
				break;
		}

		return next;
	}
}