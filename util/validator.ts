import * as jsonschema from "jsonschema";

export class Validator {
	static run(data, schema, options: any = { throwError: true }) {
		let errors: Array<jsonschema.ValidationError>;

		if ((errors = (new jsonschema.Validator()).validate(data, schema, options).errors).length) {
			return errors;
		}

		return true;
	}
}

export default Validator;