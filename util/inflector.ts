const inflector = require("i")();

export class Inflector {
	static camelize(text: string, options: any = { capitalizeFirstLetter: true }) {
		return inflector.camelize(text, options.capitalizeFirstLetter);
	}

	static underscore(text: string) {
		return inflector.underscore(text);
	}

	static singularize(text: string) {
		return inflector.singularize(text);
	}

	static pluralize(text: string) {
		return inflector.pluralize(text);
	}
}

export default Inflector;