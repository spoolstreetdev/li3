import Dispatcher from "../action/dispatcher";
import Filters from "../aop/filters";

import {Request} from "./request";
import {Response} from "./response";
import {Abstract} from "../core/handler";

export default class Function {
	static async run (handler: Abstract, params: any) {
		return Filters.run(Function, "run", { handler, params, request: new Request(handler.extract()) }, async (chain) => {
			try {
				if (!chain.response) {
					chain.response = await Dispatcher.run("invocations", {
						types: {
							request: Request,
							response: Response
						},

						request: chain.request
					});
				}

				return handler.done(chain.response);
			} catch (error) {
				return handler.error(error);
			}
		});
	}
}