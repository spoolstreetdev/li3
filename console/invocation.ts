import Filters from "../aop/filters";
import Action from "../action";

import {Response} from "../action/response";

export default abstract class Invocation extends Action {
	protected request: any;

	invoke = async () => {
		return await Filters.run(Invocation, "invoke", {}, async () => {
			return new Response({
				output: await this[this.request.params.action]()
			});
		});
	}
}