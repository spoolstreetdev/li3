export class Request {
    params: any| { invocation: string, action: string };
	data: any;

	constructor(config?: any) {
		this.init(config);
	}

	init(config: any) {
		if (config) {
			for (let key in config) {
				if (typeof config[key] != "undefined") {
					this[key] = config[key];
				}
			}
		}
	}
}