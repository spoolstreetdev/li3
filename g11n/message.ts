import {Filters} from "../aop/filters";

export class Message {
	static text(symbol, replacements?: string[]) {
		return Filters.run(this, "text", { symbol, replacements }, function(params) {
			return symbol;
		});
	}
}

export default Message;