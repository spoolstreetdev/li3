import {Filters} from "../aop/filters";
import Adaptable from "../core/adaptable";

export class Session extends Adaptable {
	read = (key?: any, options: any = {}): any => {
		return Filters.run(this.constructor, "read", { key, options: this._options(options) }, (params) => {
			if (params.key) {
				return this.get(params.key, params.options);
			}

			return this.config(params.options.name);
		});
	};

	write = (data: any, options: any = {}): any => {
		return Filters.run(this.constructor, "write", { data, options: this._options(options) }, (params) => {
			if (!this.config(params.options.name)) {
				return false;
			}

			for (let key in params.data) {
				this.set(key, params.data[key], params.options);
			}

			return this.get(params.key, params.options);
		});
	};

	clear = (options: any = {}) => {
		return Filters.run(this.constructor, "clear", { options: this._options(options) }, (params) => {
			return this.remove(params.options);
		});
	};
}

export default new Session();