import * as message from "../message";

const qs = require("qs");

const defaults = {
	protocol: null,
	version: "1.1",
	headers: {},

	_type: null
};

export const Types = {
	json: "application/json",
	form: "application/x-www-form-urlencoded"
};

export class Message extends message.Message {
	protocol: string;
	version: string;
	headers: any;

	_type: string|boolean;

	init(config: any) {
		super.init(Object.assign({}, defaults, config));

		if (config.type) {
			this.type(config.type.split(/\s*;\s*/)[0]);
		}

		if (this.host.indexOf("/") !== -1) {
			let parts = this.host.split("/");

			this.host = parts[0];
			this.path = parts[1];

			this.path = `/${this.path}`.replace(/\/\//g, "/");
		}

		this.protocol = this.protocol || `HTTP/${this.version}`;
	}

	type(type = null) {
		if (type === false) {
			delete this.headers['Content-Type'];
			this._type = false;
			return null;
		}

		if (!type && this._type) {
			return this._type;
		}

		if (!type) {
			let headers = Object.assign({}, { 'Content-Type': null }, this.headers);

			if (headers['Content-Type']) {
				type = headers['Content-Type'].split(/(\s+)?;(\s+)?/, 1)[0];
			}
		}

		if (!type) {
			return null;
		}

		return this._type = type;
	}

	render(data = null, options: any = {}) {
		let defaults = { buffer: null, encode: false, decode: false };
		options = Object.assign({}, defaults, options);

		if (!this.type() && this.headers && this.headers['Content-Type']) {
			this.type(this.headers['Content-Type'].split(/\s*;\s*/)[0]);
		}

		let body: any = this.body;

		if (!options.buffer && body === null) {
			return "";
		}

		if (options.encode) {
			body = this._encode(body);
		}

		if (options.decode) {
			body = this._decode(body);
		}

		return body;
	}

	_encode(body) {
		switch (this._type) {
			case "application/json":
				return JSON.stringify(body, null, 2);
			case "application/x-www-form-urlencoded":
				return qs.stringify(body);
		}

		return body;
	}

	_decode(body) {
		switch (this._type) {
			case Types.json:
				return JSON.parse(body);
			case Types.form:
				const data = qs.parse(body);

				if (data && typeof data === "object" && body in data) {
					throw new Error("JSON input input to qs.parse");
				}

				return data;
			default:
				for (let type in Types) {
					try {
						this.type(Types[type]);
						return this._decode(body);
					} catch (error) {
					}
				}

				break;
		}

		return body;
	}
}