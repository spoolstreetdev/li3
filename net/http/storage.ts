import Filters from "../../aop/filters";
import Throwable from "../../core/throwable";
import StaticObject from "../../core/static_object";
import Text from "../../util/text";

export default class extends StaticObject {
	protected static _config = {};
	protected static _connection: any;

	static async get(params: any = {}, attributes: any = {}) {
		return await this.run("get", Object.assign({}, { attributes, import: null }, params));
	}

	static async put(params: any = {}, attributes: any = {}) {
		return await this.run("put", Object.assign({}, { attributes, type: null, body: null, redirect: null }, params));
	}

	static async delete(params: any = {}, attributes: any = {}) {
		return await this.run("delete", Object.assign({}, { attributes }, params));
	}

	static async exists(params: any = {}, attributes: any = {}) {
		return await this.run("exists", Object.assign({}, { attributes }, params));
	}

	static async move(params: any = {}, attributes: any = {}) {
		return await this.run("move", Object.assign({}, { attributes }, params));
	}

	static async copy(params: any = {}, attributes: any = {}) {
		return await this.run("copy", Object.assign({}, { attributes }, params));
	}

	static async attributes(params: any = {}) {
		return await this.run("attributes", params);
	}

	public static publish(options) {
		let params = {
			config: this.config()
		};

		return Text.insert(!options.endpoint ? "{:endpoint}/{:bucket_name}/{:target}" : "{:endpoint}/{:target}", Object.assign({}, options, {
			endpoint: options.endpoint || params.config.storage.endpoint,
			bucket_name: params.config.storage[options.bucket]
		}));
	}

	static bucket(params, key = "bucket") {
		let Bucket;

		if (!(Bucket = params.config.storage[params[key]])) {
			throw new Error(`Bucket name ${params[key]} doesn't exist.`);
		}

		return Bucket;
	}

	static get connection(): any {
		if (this._connection) {
			return this._connection;
		}

		return Filters.run(this, "connection", { config: this.config() }, (params) => {
			return this._connection = params;
		});
	}

	protected static async run(method: string, params: any = {}, callback?: any) {
		let defaults = {
			config: this.config(),
			connection: this.connection,
			bucket: null,
			target: null
		};

		return Filters.run(this, method, Object.assign({}, defaults, params), callback || (async () => {
			throw new Throwable(`Unhandled method "${method}"`, 503);
		}));
	}
}