import * as message from "./message";

const defaults = {
	_type: "text/html",

	status: { code: 200, message: "OK" },
	encoding: "UTF-8",
	cookies: {}
};

export class Response extends message.Message {
	status: any;
	encoding: string;
	cookies: any;

	init(config: any) {
		super.init(Object.assign({}, defaults, config));
	}

	type(type = null) {
		if (type === false) {
			this._type = false;
			return null;
		}

		if (!type && this._type) {
			return this._type;
		}

		return this._type = type.split(/(\s+)?;(\s+)?/, 1)[0];
	}

	render(data = null, options = {}) {
		if (this.headers) {
			if (this.headers.Location) {
				this.status = { code: "302", message: "Found" };
			}
		}

		return super.render(data, options);
	}
}