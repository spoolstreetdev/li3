import * as message from "./message";

const defaults = {
	method: "GET",

	query: {},
	auth: null,
	cookies: {}
};

export class Request extends message.Message {
	method: string;

	query: any;
	auth?: any;
	cookies: any;

	identity: any;

	init(config: any) {
		super.init(Object.assign({}, defaults, config));
	}

	render(data = null, options = {}) {
		return super.render(data, Object.assign({}, { decode: true }, options));
	}
}