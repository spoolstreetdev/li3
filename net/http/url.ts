const qs = require("qs");
const url = require("url");
const sort_object_keys = require("sort-object-keys");

export default class {
	static sort(uri: string) {
		let params = url.parse(uri);

		if (!params.query) {
			return uri;
		}

		let parts = url.parse(uri),
			query = sort_object_keys(qs.parse(parts.query));

		delete params.search;

		return url.format(Object.assign({}, params, { query }));
	}
}