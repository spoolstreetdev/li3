export class Message {
	scheme: string = "tcp";
	host: string = "localhost";
	port: string = null;
	username: string = null;
	password: string = null;
	path: string = null;
	body: any = null;

	constructor(config: any) {
		this.init(config);
	}

	init(config: any) {
        if (config) {
            for (let key in config) {
                if (typeof config[key] != "undefined") {
                    this[key] = config[key];
                }
            }
        }
	}
}

export default Message;